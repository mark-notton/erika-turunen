var gulp				= require('gulp'),
		chalk				= require('chalk'),
		gulpif      = require('gulp-if'),
		replace	  	= require("gulp-replace"),
		sass				= require('gulp-sass'),
    notify      = require('gulp-notify'),
    plumber     = require('gulp-plumber'),
    util        = require('gulp-util'),
		uglify      = require('gulp-uglify'),
    file_concat = require('gulp-concat'),
		sourcemaps  = require('gulp-sourcemaps'),
		customProps = require('gulp-custom-props'),
    svgSprite   = require('gulp-svg-sprite'),
		browserSync = require('browser-sync').create();

// PostCSS
var postcss          = require('gulp-postcss'),
		autoprefixer     = require('autoprefixer'),
		postcssInlineSvg = require('postcss-inline-svg'),
		postcssAssets    = require('postcss-assets');

// Settings
var project			= 'Erika Turunen';
var localServer = 'www.erikaturunen.loc';
var sourceMaps  = true;
var cssOutput	  = 'nested'; // 'compressed'

// Directories
var public 		  = 'public';
var scripts 	  = 'scripts'; // Development Javascript files ( read & write )
var node    	  = 'node_modules'; // Dependency/vendor Javascript files ( read only )
var js          = public + '/assets/js/'; // Production Javascript files ( read only )
var images    	= public + '/assets/images/';
var css         = public + '/assets/css/';

// Handlers
var handlers = {
  onError: function() {
    return notify.onError({
      'icon': "https://i.imgur.com/VsfiLjV.png",
      'title': project,
      'message': '<%= error.message %>'
    });
  },
  onSuccess: function(message) {
    return notify({
      'icon': "https://i.imgur.com/G6fTWAs.png",
      'title': project,
      'message': message || 'Successful',
      'onLast': true
    });
  },
};

// Listeners/Watchers for selected files
gulp.task('serve', ['sass', 'js'], function() {
  browserSync.init({
		watchTask: true,
		open: 'external',
    proxy: localServer,
    host: localServer,
    notify: false
  });
	gulp.watch(['sass/**/*.scss'], ['sass']);
	gulp.watch(public + '/assets/images/sprites/**/*.svg', ['svg']);
	gulp.watch(scripts + '/**/*.js', ['js']).on('change', browserSync.reload);
	gulp.watch('craft/templates/**/*.html').on('change', browserSync.reload);
	gulp.watch('craft/templates/**/*.twig').on('change', browserSync.reload);
	gulp.watch('craft/plugins/**/*.php').on('change', browserSync.reload);
});

// Sass
gulp.task('sass', function() {
	var processors = [
			autoprefixer(),
			postcssAssets({loadPaths: [images]}),
			postcssInlineSvg({path:images + '/sprites'}),
	];
	return gulp.src(["sass/**/*.scss"])
	.pipe(gulpif(sourceMaps, sourcemaps.init()))
	.pipe(plumber({errorHandler: handlers.onError() }))
	.pipe(sass({outputStyle: cssOutput}))
	.pipe(customProps())
	.pipe(postcss(processors))
	.pipe(gulpif(sourceMaps, sourcemaps.write()))
	.pipe(gulp.dest(css))
	.pipe(browserSync.stream())
	.pipe(handlers.onSuccess('SASS Compiled Successfully') );
});

// Compile Javascript into a minified file & auto-inject into browser
gulp.task('js', function(){
  return gulp.src([
		scripts + '/browsers-and-devices.js',
		scripts + '/fixers.js',
		scripts + '/internet-explorer.js',
		scripts + '/ui.js',
		scripts + '/blog.js',
		scripts + '/instagram.js',
		scripts + '/model.js',
		scripts + '/visibilitychange.js',
		scripts + '/work.js',
		scripts + '/main.js'
  ])
	.pipe(gulpif(sourceMaps, sourcemaps.init()))
	.pipe(plumber({errorHandler: handlers.onError() }))
	.pipe(file_concat('main.min.js'))
	.pipe(uglify({ mangle: false }))
	.pipe(gulpif(sourceMaps, sourcemaps.write('maps')))
	.pipe(gulp.dest(js))
	.pipe(browserSync.stream())
	.pipe(handlers.onSuccess('Main JS Compiled Successfully'));
});

// Manual task for building core file from all vendor files
gulp.task('core', function(){
  return gulp.src([
		node + '/jquery/dist/jquery.js',
		node + '/gsap/src/uncompressed/TweenMax.js',
		node + '/gsap/src/uncompressed/TimelineMax.js',
		node + '/gsap/src/uncompressed/plugins/ScrollToPlugin.js',
		node + '/fastclick/lib/fastclick.js',
		node + '/jquery-placeholder/jquery.placeholder.js',
		node + '/on-window-resize/on-window-resize.js',
		node + '/breakpoint-checker/breakpoint-checker.js',
		node + '/class-looper/class-looper.js',
		node + '/on-page-checker/on-page-checker.js',
		node + '/temporary-class/temporary-class.js',
		node + '/jquery-fracs/dist/jquery.fracs.js',
		node + '/jquery-mousewheel/jquery.mousewheel.js',
		node + '/vide/dist/jquery.vide.js',
		node + '/instafeed.js/instafeed.js',
		node + '/withinviewport/jquery.withinviewport.js',
		node + '/hammerjs/hammer.js',
		node + '/custom-props/dist/custom-props.js',
		node + '/animation-listener/animation-listener.js',
		// node + '/jquery.unevent.js/jquery.unevent.js',
		node + '/custom-props-extension/custom-props-extension.js',
		node + '/replace-tags/replace-tags.js'
  ])
	.pipe(gulpif(sourceMaps, sourcemaps.init()))
	.pipe(plumber({errorHandler: handlers.onError() }))
	.pipe(file_concat('core.min.js'))
	.pipe(uglify({ mangle: false }))
	.pipe(gulpif(sourceMaps, sourcemaps.write('maps')))
	.pipe(gulp.dest(js))
	.pipe(browserSync.stream())
	.pipe(handlers.onSuccess('Core JS Compiled Successfully'));
});

// SVG SPRITES GENERATOR
gulp.task('svg', function(){
  // SYMBOLS
  gulp.src(public + '/assets/images/sprites/**/*.svg')
  .pipe(svgSprite({
    mode: {
      symbol: {
        sprite: 'sprite-symbols.svg',
        dest: '',
        example: false,
        render : {
          scss : {
            dest:'../../../sass/base/_svg-symbols.scss',
          }
        },
        prefix: '.svg-%s',
        dimensions : '-size',
        example: false,
      },
    }
  }))
	.pipe(plumber({errorHandler: handlers.onError() }))
  .pipe(replace("<svg","<svg style='display:none !important;'"))
  .pipe(gulp.dest(public + '/assets/images'))
	.pipe(handlers.onSuccess('SVG Symbols generated Successfully'));
});

// All tasks
gulp.task('default', ['sass', 'js', 'core', 'svg'], function(){
	return util.log(chalk.green("All gulp tasks completed"));
});
