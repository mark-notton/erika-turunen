/// Transition mixin handles animation for any number of transitions in any format/order
///
/// @author Mark Notton
///
/// @access public
///
/// @todo Stagger option. Make it so each transition happens one after the other, regardless of delay
///
/// @param {arglist} $args - Pass in the following transtion settings, in any order.
///   [property]  - width, margin, rotate, skew, color, etc...
///   [delay]     - delay times are always calculated in seconds (not miliseconds). You do not have to define the 's'
///   [ease-type] - ease-in, ease-in-out etc... You can also refer to the eases variables. $easeInSine, $easeInOutBack
///   [duration]  - duration times are always calculated in seconds (not miliseconds). You do not have to define the 's'
///   [direction] - forwards, default, backwards, reverse. direction should be in it's own argment, not part of a list of transition settings. It can however be in the same argument as 'stagger'.
///                 Directions can be backwards or forwards (default). Setting it backwards will reverse the order
///                 of the delays if more than one argument has been made.
///   [stagger]   - stagger should be in it's own argment, not part of a list of transition settings. It can however be in the same argument as 'direction'.
///                 Stagger can be defined just by passing the string 'stagger'. This will add previous transition delays and directions to the current
///                 transtion so each argument is called one after the other.
///   [off]       - passing off within any argument will ignore everything and set the transition to 'initial'. Essentially disabling all transitions
///
/// @require {Mixin} contains
/// @require {Mixin} prepend
/// @require {Mixin} prefixer
/// @require {Variables} eases - optional
///
/// @example scss - Usage
/// .foo {
///   @include transition(
///      background-color 0 ease-in,
///      border-radius 0.5 ease-out,
///      rotate 1 1 $easeInOutBack,
///      reverse
///   );
/// }
///
/// @example css - CSS output
/// .foo {
///   -webkit-transition: background-color 0.2s ease-in 0s, border-radius 0.2s ease-out 0.5s, -webkit-transform 1s cubic-bezier(0.68, -0.55, 0.265, 1.55) 1s;
///   transition: background-color 0.2s ease-in 0s, border-radius 0.2s ease-out 0.5s, transform 1s cubic-bezier(0.68, -0.55, 0.265, 1.55) 1s; }
/// }
///

@mixin transition($settings...) {

  // Default Settings
  $direction : forwards;
  $stagger : false;

  // Private Settings
  $transitions : ();
  $transitions-webkit : ();
  $animations  : ();
  $delays : ();
  $eases : ();
  $durations : ();
  $off : false;

  // Quick check to make sure anything at all has been passed.
  // If not, set 'all' as the default transition
  // $args: if(length($args) > 0, $args, all);

  $settings-list : _arglist-to-list($settings);

  // Loop through and check all arguments for any special options
  @each $settings in $settings-list {
    @if contains($settings, forwards default backwards reverse) {
      $direction : $settings;
    } @elseif contains($settings, stagger) {
      $stagger : true;
    } @elseif contains($settings, off) {
      $off : true;
    }
  }

  @if $off == true {
    @include prefixer(transition, initial);
  }

  // Loop through and set all the necessary settings for each animation set
  @each $settings in $settings-list {

    // Default Settings
    $animation : ();
    $duration : 0.2;
    $ease : ease-in-out;
    $delay : 0;

    // Private Settings
    $excluded : false;
    $value-counter : 0;

    @each $setting in $settings {
      // Check a range of special rules. Exclude this transition if this is true.
      @if contains($setting, forwards default backwards reverse stagger) {
        $excluded : true;
      } @else {
        // Checks for 'strings'
        @if type-of($setting) == string {
          @if contains(str-slice(#{$setting}, 0, 4), ease line step cubi init) {
            // If the first 4 characters of the string match the first 4 characters
            // of a ease type, update the ease variable
            $ease : $setting;
          } @elseif contains($setting, transform matrix translate translateX translateY scale scaleX scaleY rotate skew skewX skewY matrix3d translate3d translateZ scale3d scaleZ rotate3d rotateX rotateY rotateZ perspective) {
            // If the string mataches a transform type, define the animation as a transform
            $animation : append($animation, transform);
          } @else {
            $animation : append($animation, $setting);
          }
        }
        // Checks for 'numbers'
        @if type-of($setting) == number {
          @if $value-counter < 1 {
            // Add the first instance of a number as the delay
            $delay : $setting;
            $value-counter : $value-counter + 1;
          } @elseif $value-counter >= 1 {
            // Add the second instance of a number as the duration
            $duration : $setting;
          }
        }
      }
    }

    @if not $excluded {
      // Quick check to make sure animation has a fallback if none were defined
      $animations : if(length($animation) == 0, append($animations, all), append($animations, $animation));
      // Quick check for the direction rule. If backwards or reversed, add delays the the start of the $delays list
      $delays     : if(contains($direction, backwards reverse), prepend($delays, $delay), append($delays, $delay));
      $eases      : append($eases, $ease);
      $durations  : append($durations, $duration);
    }
  }

  // Loop through all the animations and add the transition to the overall transitions list
  @for $i from 1 through length($animations) {
    $animation   : nth($animations, $i);
    $duration    : nth($durations, $i);
    $ease        : nth($eases, $i);
    $delay       : nth($delays, $i);
    @each $ani in $animation {
      $transition  : ($ani #{$duration + 's'} $ease #{$delay + 's'});
      $transitions : append($transitions, $transition, comma);

      $transition-webkit  : (if($ani == transform, -#{webkit}-#{$ani}, $ani) #{$duration + 's'} $ease #{$delay + 's'});
      $transitions-webkit : append($transitions-webkit, $transition-webkit, comma);
    }
  }

  @include prefixer(transition, $transitions-webkit, webkit only);
  @include prefixer(transition, $transitions, null);

}

/// @alias transition
@mixin ani($settings...) { @include transition($settings...); }

@mixin transition-timing-function($ease:ease-in-out) {
  @include prefixer(transition-timing-function, $ease, webkit);
}

@mixin transition-easing($ease:ease-in-out) {
  @include transition-timing-function($ease);
}
