var modal = {
  gallery : $('.gallery'),
  current : null,
  init : function() {
    modal.gallery.find('image-element').click(function(event){
      if (!$('modal-container').length) {
        modal.create(this);
      }
    });

    $( "page-section" ).on( "click", "modal-container .background", function() {
      modal.remove();
    });

    $( "page-section" ).on( "click", "modal-container nav button.prev", function() {
      modal.prevButton();
    });

    $( "page-section" ).on( "click", "modal-container nav button.next", function() {
      modal.nextButton();
    });
  },
  create : function($this) {
    // $(this).data('id')
    $current = $($this).index();
    $images = $($this).siblings('image-element').andSelf();

    $template = '<modal-container>';
    $template = $template + '<div class="background"></div>';

    $images.each(function(i) {
      if (i == $current) {
        $template = $template + '<image-element class="current" style="background-image:url('+$(this).data('id')+')"></image-element>';
      } else {
        $template = $template + '<image-element style="background-image:url('+$(this).data('id')+')"></image-element>';
      }
    });

    $template = $template + '<nav><button class="prev"></button><button class="next"></button></nav>';
    $template = $template + '</modal-contaier>';

    $($this).parents('page-section').append($template);

    modal.current = $('modal-container');

    $('nav.main').addClass('hide');

    modal.prevNextCheck();

    setTimeout(function() {
       modal.current.addClass('intro');
    }, 100);
  },
  remove : function() {
    modal.current.removeClass('intro');
    setTimeout(function() {
      modal.current.remove();
      modal.current = null;
      $('nav.main').removeClass('hide');
    }, 1000);
  },
  prevButton : function() {
    // console.log('prevButton');
    $this = modal.current.find('.current');
    $this.removeClass('current').prev().addClass('current');
    modal.prevNextCheck();

  },
  nextButton : function() {
    // console.log('nextButton');
    $this = modal.current.find('.current');
    $this.removeClass('current').next().addClass('current');
    modal.prevNextCheck();
  },
  prevNextCheck : function() {
    $current = modal.current.find('.current').index();
    $total = modal.current.find('image-element').length;

    if ($current == 1) {
      $('modal-container nav button.prev').addClass('hide');
    } else if ($current == $total) {
      $('modal-container nav button.next').addClass('hide');
    } else {
      $('modal-container nav button.next, modal-container nav button.prev').removeClass('hide');
    }
  }
}
