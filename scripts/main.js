var single = $('body').attr('data-single-page') == 'true' ? true : false;
var locale = $('html').attr('lang');
var breaks = {
  'max': 1600,
  'large': 1080,
  'medium': 780,
  'small': 480,
  'min': 320
};

;(function ($) {
    var on = $.fn.on, timer;
    $.fn.on = function () {
        var args = Array.apply(null, arguments);
        var last = args[args.length - 1];

        if (isNaN(last) || (last === 1 && args.pop())) return on.apply(this, args);

        var delay = args.pop();
        var fn = args.pop();

        args.push(function () {
            var self = this, params = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                fn.apply(self, params);
            }, delay);
        });

        return on.apply(this, args);
    };
}(this.jQuery || this.Zepto));

// (function( $ ) {
//
//   // Add the selector, function, and breakpoint rules into an array, to be called when the screen resizes
//   $.fn.extend({
//     classLooper : function(options) {
//
// 	    var defaults = {
// 	        fullduration: null,
// 					first: null,
// 					delay: 0,
// 	        duration: 2,
// 	        loop: 3,
// 					reverse:false,
// 					class:'current'
// 	    };
//
// 	    var settings = $.extend( {}, defaults, options ),
// 					current = 1,
// 					counter = 0,
// 					looped = 1,
// 					timer = null,
// 					elements = this;
//
// 			if (this.length) {
//
// 				settings['total'] = elements.length;
//
// 				// Check if the fullduration option has been set to a number.
// 				// If it has, devide this by the amount of elements there are to loop through.
// 				// This will overright the durations option.
// 				if (typeof settings.fullduration == 'number') {
// 					settings.duration == settings.fullduration/elements.length;
// 				}
//
// 				// If the first option is a number, and is not greater than the amount of elements
// 				// The set that specific element with the defined class name.
// 				if (typeof settings.first == 'number' && settings.first <= settings.total) {
// 					elements.removeClass(settings.class).eq(settings.first - 1).addClass(settings.class);
// 				}
//
// 				// Delay the initial start of the timers
// 				setTimeout(function() {
//
// 					if ( !settings.reverse ) {
// 						// Forwards
// 						timer = setInterval(function() {
// 							console.log(elements[counter]);
// 							// $(elements).find(settings.class).removeClass(settings.class);
// 							$(elements[counter]).removeClass(settings.class);
// 							counter ++;
// 							if ( counter == settings.total) {
// 								counter = 0;
// 								if (looped == settings.loop) {
// 									clearInterval(timer);
// 								} else {
// 									looped ++;
// 								}
// 							}
//
// 							// elements.eq(counter).removeClass(settings.class);
// 							// console.log(counter + '/' + settings.total, looped + '/' + settings.loop)
// 							// console.log(elements.find(settings.class));
// 							// elements.find(settings.class).removeClass(settings.class).next()
// 						}, settings.duration * 1000);
//
// 					} else {
// 						// Reversed
// 					}
//
//
//
// 					// return elements.each(function(i, e) {
// 					// 	setTimeout(function() {
// 					//
// 					// 	}, (settings['duration'] * i) * 1000 );
// 					// });
// 				}, settings.delay*1000);
//
// 			} else {
// 				console.warn('There were no elements found');
// 			}
//
//     }
//
//   });
//
// }( jQuery ));

// Initialisations 1
$(function() {

	// $('slider-container slider-slide').classLooper({ loop : 1, total:10, first:1});

	// IE Functions
  if (browser.name == 'ie') {
		internetExplorer.init();
	}
  //
	ui.init();
	fixers.init();
	modal.init();

	$('nav.main').onPage(1000, function() {
		$(this).addClass('show').addTemporaryClass("flash", 1, 1);
	});

	$('page-section#work').onPage(work.init);
	$('page-section#instagram').onPage(instagram.init);
	$('page-section#blog').onPage(blog.init);
  $('page-section#introduction').onPage(2000, function() {
		var $this = $(this);
    $this.addClass('loaded');
		var images = $(this).find('background-image');
		if (images.length > 1) {
			var current = 0;
			var duration = 5; // of each slide in seconds
			images.first().addClass('show');
	    setInterval(function(){
				current = current + 1 == images.length ? 0 : current + 1;
				images.removeClass('show').eq(current).addClass('show');
			}, duration*1000);
		}
  });

	if (desktop()) {
		visibilitychange.init();
	}

});
