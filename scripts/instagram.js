var instagram = {
	gallery : $('instagram-gallery'),
	init : function() {

		var userId = instagram.gallery.data('user-id');
		var accessToken = instagram.gallery.data('access-token');

		var userFeed = new Instafeed({
			get: 'user',
			userId: userId,
			target:'instafeed',
			accessToken: accessToken,
			limit:(mobile ? 8 : 19),
			template: '<a href="{{link}}"><img alt="{{caption}}" src="{{image}}" /><span class="likes">{{ likes }}</span></a>',
			error:function() {
				$('#instagram').prepend('<background-image class="fallback"></background-image>');
			},
			success:function(data) {
				$('#instagram').prepend('<background-image style="background-image:url('+ data.data[0]['images']['standard_resolution']['url'] +')"></background-image>');
			}
		});
		userFeed.run();
	}
}
