var internetExplorer = {
	init : function() {
		internetExplorer.placeholder();
	},
	placeholders : function() {
		$('form').onPage(function() {
			$('input, textarea').placeholder();
		});
	}
};
