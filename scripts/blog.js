// Blog
var blog = {
  container    : $('page-section#blog'),
  list    : $('page-section#blog blog-list'),
  button : $('page-section#blog blog-list article button'),
  current : 1,
  offset  : 0,
  limit   : null,
  pages   : null,
  total   : null,
  viewing : null,
  more    : null,
  disabled: false,
  init: function() {

    blog.limit   = blog.list.data('limit');
    blog.offset = blog.limit;
    blog.pages   = blog.list.data('total-pages');
    blog.total   = blog.list.data('total');
    blog.viewing = blog.list.siblings('viewing-counter');
    blog.more  = blog.list.siblings('button.show-more');

    blog.more.on('click', function(event) {
      event.preventDefault();
      if(blog.disabled == false && blog.current != blog.pages) {
        blog.showmore();
      }
    });

    blog.button.onPage(function() {
      blog.button.on('click', function(event) {
        event.preventDefault();
        blog.load($(this));
      });
    });

  },

  load : function(button) {

    var id = button.attr('data-id');

    button.toggleClass('loading');
    button.parents('blog-list').toggleClass('loading');
    $.ajax({
      method:'GET',
      url:'/blog/_content?id='+id+'&locale='+locale,
      success:function(response){
        if(!response.length) { return null; }
        $('body').addClass('disable');
        button.removeClass('loading');
        if ( blog.container.append(response) ) {
          fixers.videoIframes();
          setTimeout(function() {
             $('blog-container').addClass('intro');

             blog.containerSettings();
          }, 500);
        }

      },
      error:function(response){
        console.log("ERROR", response);
      }
    });
  },

  containerSettings: function() {
    // Close Buttons
    $('blog-container button.close-text, blog-container container-background').on('click', function(e) {
      e.preventDefault();
      $('body').removeClass('disable');
      blog.button.parents('blog-list').removeClass('loading');
      $('blog-container').removeClass('intro');
      setTimeout(function() {
        $('blog-container').remove();
      }, 2000);
    });

  },

  showmore : function() {

    blog.more.addClass('loading');
    blog.disabled = true;

    $.ajax({
      method:'GET',
      url:'/blog/_loader?limit='+blog.limit+'&offset='+blog.offset+'&single='+(single ? '1' : '2')+'&locale='+locale,
      success:function(response){
        if(!response.length) { return null; }
        // Add to current count
        blog.current ++;
        // Remove loading and add content
        blog.more.removeClass('loading');
        // Add new content to list
        blog.list.append(response);
        // Removed the closed classes
        var newBlogs = blog.list.find('article.new');

        TweenMax.staggerFrom($(newBlogs), 1, {opacity:0, y:50, ease:Back.easeOut, onComplete:function() {
          $(this.target).removeClass('new');
        }}, 0.3);

        // TweenMax.to(window, 1, {scrollTo:$(newBlogs).first().offset().top - 60, ease:Power1.easeInOut });

        // Update the offset
        blog.offset = blog.offset + blog.limit;
        // Get the amount of articles
        var showing = blog.list.find('article').length;
        // Update the counter attribute
        blog.list.attr('data-count', showing);
        // Update the viewing text
        if(blog.current == blog.pages) {
          blog.list.addClass('no-more-entries');
        }
        blog.viewing.text("Viewing "+showing+" of "+blog.total+" posts");

        blog.disabled = false;

        blog.button = $('page-section#blog blog-list article button');

        blog.button.on('click', function(event) {
          event.preventDefault();
          blog.load($(this));
        });

        ui.setHeight();

      },
      error:function(response){
        console.log("ERROR", response);
      }
    });
  }
};
