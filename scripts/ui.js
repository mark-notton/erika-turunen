var ui = {
  current : $('page-section:first'),
  sections : $('page-section'),
  nav : $('nav.main'),
  buttons : $('nav.main a'),
  menu : $('nav.main .menu'),
  prev : $('nav.main .previous'),
  next : $('nav.main .next'),
  wrapper : $('main-wrapper'),
  sitename : document.title.split(' | ')[1],
  transition : 1,
  animating : false,
  init : function() {
		if ( !single ) {

	    $(window).on('load', function() {
	      $(this).scrollLeft(ui.current.position().left);
	    });

			ui.arrowkeys();
			ui.detection();
			ui.afterscroll();
			ui.resize();
			// ui.mousescroll();
		}

    $(window).onWindowResize(function() {
      ui.nav.removeClass('open');
    });

		ui.navigation();
  },
  navigation : function() {

		if ( !single ) {
	    // Go to sections when navigation links are clicked on
	    ui.buttons.click(function(event) {
	      event.preventDefault();
	      var $page = $($(this).attr('href'));
	      ui.nav.removeClass('open');
	      // if (!ui.animating || event.originalEvent !== undefined) {
	        $position = new Object();
	        $position[breakpoint('medium') ? 'y' : 'x'] = (breakpoint('medium') ? $page.position().top : $page.position().left);
          var old = ui.current;
	        TweenMax.to(window, ui.transition, {scrollTo:$position, ease:Power1.easeInOut, onComplete:function() {
            old.scrollTop(0);
          }});

	      //   TweenMax.to(window, ui.transition, {scrollTo:$position, ease:Power1.easeInOut,
	      //     onStart:function() {
	      //       ui.animating = true;
	      //     },
	      //     onComplete:function() {
	      //       ui.animating = false;
	      //     }
	      //   });
	      // }
	    });
      // Previous
      ui.prev.click(function(event) {
        event.preventDefault();
        ui.prevSection();
      });
      // Next
      ui.next.click(function(event) {
        event.preventDefault();
        ui.nextSection();
      });
		} else {
      ui.tone();
    }

    ui.menu.click(function(event) {

      event.preventDefault();

      console.log('scroll to bottom');

      if ( !ui.nav.hasClass('open') && !breakpoint('medium')) {
        TweenLite.to(ui.current, 1, {scrollTo:{y:"max"}});
      }

      ui.nav.toggleClass('open');
    });
  },
  arrowkeys : function() {
    // Detect left or right arrow key presses.
    $(document).keydown(function(e) {
      if (breakpoint('medium')) {
        switch(e.which) {
          case 38: // left
            ui.prevSection();
          break;
          case 40: // right
            ui.nextSection();
          break;
          default: return;
        }
      } else {
        switch(e.which) {
          case 37: // left
            ui.prevSection();
          break;
          case 39: // right
            ui.nextSection();
          break;
          default: return;
        }
      }
      e.preventDefault();
    });
  },
  nextSection : function() {
    // Go to the next section
    // if (!ui.animating ) {
      var $next = ui.buttons.filter('.current').next();
      if ( $next.length > 0 ) {
        $next.trigger("click");
      } else if (ui.buttons.filter('.current').parent().is(":first-of-type")) {
        ui.buttons.parent().nextAll('div').find('a:first-of-type').trigger("click");
      }
      // console.log('right');
    // }
  },
  prevSection : function() {
    // Go to the previous section
    // if (!ui.animating ) {
      var $prev = ui.buttons.filter('.current').prev();
      if ( $prev.length > 0 ) {
        $prev.trigger("click");
      } else if (ui.buttons.filter('.current').parent().is(":last-of-type")) {
        ui.buttons.parent().prevAll('div').find('a:last-of-type').trigger("click");
      }
      // console.log('left');
    // }
  },
  detection : function() {
    // Use Fracs to detect what page section is the most likely current section when scrolling
    $.each(['possible', 'visible', 'viewport'], function(i, value) {
      ui.sections.fracs('max', value, function (best) {
        ui.current = $(best);
        // TweenMax.to(old, ui.transition, {scrollTo:0, ease:Power1.easeInOut});
        $('page-section, nav.main a').removeClass('current');
        ui.current.addClass('current');
        ui.tone();
        // ui.setHeight();
        ui.buttons.eq(ui.current.index()).addClass('current');

        ui.title();

        if ($('work-container button.close').length && !breakpoint('medium')) {
          $('work-container button.close').trigger('click');
        }

        if ( ui.wrapper.find('.current').index() == 0) {
          ui.prev.addClass('disabled');
        } else if (ui.wrapper.find('.current').index() == ui.sections.length - 1) {
          ui.next.addClass('disabled');
        } else {
          ui.prev.removeClass('disabled');
          ui.next.removeClass('disabled');
        }

      });
    });
  },
  setHeight : function() {
    ui.wrapper.css('height', 'calc(' + Math.ceil($('page-section.current main-content').innerHeight()) + 'px + 20vh)');
  },
  afterscroll : function() {
    // After the user has manually scrolled, this function will be called and align the current section within the viewport
    $(window).on('scroll', function() {
      if (!breakpoint('medium')) {
        $position = new Object();
        $position[breakpoint('medium') ? 'y' : 'x'] = (breakpoint('medium') ? ui.current.position().top : ui.current.position().left);
        ui.nav.removeClass('open');
        TweenMax.to(window, ui.transition, {scrollTo:$position, ease:Power1.easeInOut,
          onStart:function() {
            ui.animating = true;
          },
          onComplete:function() {
            ui.animating = false;
          }
        });
      }
    }, 300);
  },
  resize : function() {
    // When the screen resolution resizes, instantly aligned the current page section so it doesn't offset
    $(window).onWindowResize(function() {
      $(window).scrollLeft(ui.current.position().left);
    });
  },
  title : function() {
    var title = ui.current.find('h1:first').text();
    if ( title == ui.sitename ) {
      document.title = ui.sitename;
    } else {
      document.title = ui.current.find('h1:first').text() + ' | ' + ui.sitename;
    }
  },
  tone : function() {
    if (ui.current.attr('data-tone')) {
      ui.buttons.parents('nav').attr('data-tone', ui.current.attr('data-tone'));
    } else {
      ui.buttons.parents('nav').attr('data-tone', 'light');
    }
  },
  mousescroll : function() {
    // Make it so the vertical mouse scroll makes the page scroll horizontally
    $("body").mousewheel(function(event, data) {
      if (!breakpoint('medium')) {
        event.preventDefault();
        this.scrollLeft -= (data * 5);
      };
    });
  }
};
