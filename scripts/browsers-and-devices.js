var browser = {
  name : $('html').data('browser').split(' ')[0],
  version : $('html').data('browser').split(' ')[1]
};

var mobile = function(tablet) {
  if (typeof tablet !== undefined && tablet === true) {
    return $('body').hasClass('tablet') || $('body').hasClass('mobile');
  } else {
    return $('body').hasClass('mobile');
  }
};

var tablet = function() {
  return $('body').hasClass('tablet');
};

var desktop = function() {
  return $('body').hasClass('desktop');
};
