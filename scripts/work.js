// Work
var work = {
  container : $('page-section#work'),
  lists : $('#work main-content .lists section'),
  button : $('#work main-content .lists section ul button'),
  navigation : $('#work main-content > nav button'),
  init: function() {
    work.button.click(function(e) {
      e.preventDefault();
      work.load($(this));
    });
    work.navigation.click(function(e) {
      e.preventDefault();
      work.navigate($(this));
    });
  },
  load : function(button) {

    var id = button.attr('data-id');

    button.toggleClass('loading');
    button.parents('nav').toggleClass('loading');
    $.ajax({
      method:'GET',
      url:'/work/_content?id='+id+'&locale='+locale,
      success:function(response){
        if(!response.length) { return null; }
        $('body').addClass('disable');
        button.removeClass('loading');
        if ( work.container.prepend(response) ) {
          TweenMax.to(ui.current, 1, {scrollTo:{y:0}});
          setTimeout(function() {
             $('work-container').addClass('intro');
             work.containerSettings();
          }, 500);
        }

      },
      error:function(response){
        console.log("ERROR", response);
      }
    });
  },
  navigate: function(category) {
    var categoryNumber = category.index(),
        categoryListsToHide = work.lists.not(':nth-of-type('+(categoryNumber)+')'),
        categoryList = categoryNumber == 0 ? work.lists : work.lists.eq(categoryNumber - 1);

    category.siblings().removeClass('select');
    category.addClass('select');

    work.container.addClass('disabled');

    TweenMax.to(categoryList, 0.5, {height:categoryList.find('> div').height() + 5, opacity:1, ease:Power1.easeInOut, onComplete:function() {
      $(this.target).css('height', '');
      TweenMax.staggerTo($(this.target).find('li'), 0.3, {opacity:1, y:0, ease:Back.easeOut}, 0.05, function() {
        work.container.removeClass('disabled');
      });
    }});

    if (categoryNumber != 0) {
      TweenMax.to(categoryListsToHide, 0.5, {height:0, opacity:0, delay:0.5, ease:Power1.easeInOut});
      TweenMax.staggerTo(categoryListsToHide.find('li'), 0.3, {opacity:0, y:-50, ease:Back.easeIn}, 0.05);
    }

  },
  containerSettings: function() {

    var total = $('.large-gallery image-element').length,
        largeGallery = $('.large-gallery');

    // Close Buttons
    $('work-container button.close-text, work-container container-background').on('click', function(e) {
      e.preventDefault();
      $('body').removeClass('disable');
      work.button.parents('nav').removeClass('loading');
      $('work-container').removeClass('intro');
      setTimeout(function() {
        $('work-container').remove();
      }, 2000);
    });

    // Thumbs
    $('work-container .mini-gallery image-thumb').on('click', function(e) {
      e.preventDefault();
      var $this = $(this);
      TweenMax.to($('work-container article'), 0.5, {scrollTo:0, ease:Power1.easeInOut, onComplete:function() {
        largeGallery.addClass('show-gallery-nav').attr('data-current', $this.index() + 1);

        $('work-container article').css('overflow-y','hidden');
        if ( $this.index() + 1 == total ) {
          largeGallery.addClass('last');
        }
      }});

    });

    // Next Button
    $('work-container nav button.next').on('click', function() {
      var current = parseInt(largeGallery.attr('data-current'));
      if ( current == total - 1) { largeGallery.addClass('last') } else { largeGallery.removeClass('first, last') }
      largeGallery.attr('data-current', current + 1);
    });

    // Previous Button
    $('work-container nav button.prev').on('click', function() {
      var current = parseInt(largeGallery.attr('data-current'));
      if ( current == 1) {
        largeGallery.removeClass('show-gallery-nav');
        $('work-container article').css('overflow-y','scroll');
      } else {
        largeGallery.removeClass('first, last');
      }
      largeGallery.attr('data-current', current - 1);
    });

    // Close Gallery
    $('work-container button.close').on('click', function(e) {
      largeGallery.attr('data-current', 0);
      largeGallery.removeClass('show-gallery-nav');
      $('work-container article').css('overflow-y','scroll');
    });
  }
};
