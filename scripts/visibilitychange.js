var visibilitychange = {
  init : function() {
    $(document).on('visibilitychange', function() {
      if(document.visibilityState == 'hidden') {
        // console.log('page is hidden');
      } else {
        // console.log('page is visible');
        $('nav.main').addTemporaryClass("flash", 1);
      }
    });
  }
}
