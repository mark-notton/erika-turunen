<?php
namespace Craft;

class EditionPlugin extends BasePlugin {
  public function getName() {
    return Craft::t('Craft Edition');
  }

  public function getVersion() {
    return '0.1';
  }

  public function getSchemaVersion() {
    return '0.1';
  }

  public function getDescription() {
    return 'Change the licence type of the Craft CMS.';
  }

  public function getDeveloper() {
    return 'Yello Studio';
  }

  public function getDeveloperUrl() {
    return 'http://yellostudio.co.uk';
  }

  public function prepSettings($settings)  {
    $sql = sprintf('UPDATE craft_info SET edition = %d, dateUpdated = NOW()', $settings['edition']);
    $db_command = craft()->db->createCommand($sql);
    $affected_rows = $db_command->execute();
  }

  public function getSettingsHtml() {
    return craft()->templates->render('edition/settings', array(
      'settings' => $this->getSettings()
    ));
  }

  protected function defineSettings() {
    return array(
      'edition' => array(AttributeType::Number, 'default' => 0),
    );
  }

  function init() {
    if (craft()->userSession->isLoggedIn() && craft()->request->isCpRequest() && !craft()->request->isAjaxRequest()) {
      craft()->templates->includeCss('#upgradepromo, #alerts { display:none !important; }');
    }
  }
}
