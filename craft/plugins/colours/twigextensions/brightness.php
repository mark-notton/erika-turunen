<?php
namespace Craft;

use Twig_Extension;
use Twig_Filter_Method;

class brightness extends \Twig_Extension {

	public function getName()	{
		return Craft::t('Brightness');
	}

	public function getFunctions() {
		return array(
			'darken'  => new \Twig_Function_Method($this, 'darken'),
			'lighten' => new \Twig_Function_Method($this, 'lighten'),
			'dark' 	  => new \Twig_Function_Method($this, 'dark'),
			'light'   => new \Twig_Function_Method($this, 'light'),
			'light_or_dark' => new \Twig_Function_Method($this, 'light_or_dark'),
			'rgba'    => new \Twig_Function_Method($this, 'hexToRgba'),
			'colour'  => new \Twig_Function_Method($this, 'hexToRgba')
		);
	}

  public function getFilters() {
    return array(
      'light_or_dark' => new Twig_Filter_Method( $this, 'light_or_dark'),
      'shades' => new Twig_Filter_Method( $this, 'shades'),
      'lighter_shades' => new Twig_Filter_Method( $this, 'lighter_shades'),
      'darker_shades' => new Twig_Filter_Method( $this, 'darker_shades')
    );
  }


	// Convert Hex to RGBA
	// http://mekshq.com/how-to-convert-hexadecimal-color-code-to-rgb-or-rgba-using-php/
	public function hexToRgba($color, $opacity = false) {

		//Return default if no color provided
		if(empty($color)) {
	    return false;
		}

		//Sanitize $color if "#" is provided
    if ($color[0] == '#' ) {
    	$color = substr( $color, 1 );
    }

    //Check if color has 6 or 3 characters and get values
    if (strlen($color) == 6) {
      $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
    } elseif ( strlen( $color ) == 3 ) {
      $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
    } else {
      return false;
    }

    //Convert hexadec to rgb
    $rgb =  array_map('hexdec', $hex);

    //Check if opacity is set(rgba or rgb)
    if($opacity){
    	$opacity = abs($opacity) > 1 ? 1.0 : $opacity;
    	$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
    } else {
    	$output = 'rgb('.implode(",",$rgb).')';
    }

	  //Return rgb(a) color string
	  return $output;
	}

	// Returns true is the colour is light, false if dark
	// {{ light('#0000ff') }}
	public function light($color)	{
		return $this->_light_or_dark($color);
	}

	// Returns true is the colour is dark, false if light
	// {{ dark('#0000ff') }}
	public function dark($color)	{
		return !$this->_light_or_dark($color);
	}

	// If colour is light, return second param (white by default). Dark and return third param (black by default)
	// {{ light_or_dark('#0000ff', 'black', 'white') }}
	public function light_or_dark( $color, $light = '#FFFFFF', $dark = '#000000' ) {
		if ( gettype($light) == 'boolean' ) {
			return $this->_light_or_dark($color) ? 'light' : 'dark';
		} else {
			return $this->_light_or_dark($color) ? $dark : $light;
		}
	}

	// Checks if a colour is light or dark
	private function _light_or_dark( $color ) {

    $hex = str_replace( '#', '', $color );

    $c_r = hexdec( substr( $hex, 0, 2 ) );
    $c_g = hexdec( substr( $hex, 2, 2 ) );
    $c_b = hexdec( substr( $hex, 4, 2 ) );

    $brightness = ( ( $c_r * 299 ) + ( $c_g * 587 ) + ( $c_b * 114 ) ) / 1000;

    return $brightness > 155;
  }

	// Lighten a hex colour
	// {{ lighten('#0000FF', 20) }}
	public function lighten($hex, $amount)	{
		$amount = ($amount < 1 ? 0 : ($amount > 100 ? 100 : $amount)) * 2.55;
		return $this->brightness($hex, $amount);
	}

	// Darken a hex colour
	// {{ darken('#0000FF', 20) }}
	public function darken($hex, $amount)	{
		$amount = ($amount < 1 ? 0 : ($amount > 100 ? 100 : $amount)) * 2.55;
		$amount = -1 * abs($amount);
		return $this->brightness($hex, $amount);
	}

	// Manipulates a colours brightnes/darkness
	private function brightness($hex, $steps) {
    // Steps should be between -255 and 255. Negative = darker, positive = lighter
    $steps = max(-255, min(255, $steps));

    // Normalize into a six character long hex string
    $hex = craft()->colours->validate_hex($hex);

    // Split into three parts: R, G and B
    $color_parts = str_split($hex, 2);
    $return = '#';

    foreach ($color_parts as $color) {
        $color   = hexdec($color); // Convert to decimal
        $color   = max(0,min(255,$color + $steps)); // Adjust color
        $return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
    }

    return $return;
	}

	// Returns array of colour shades based a single hex colour
	// {{ entry.themeColour|shades('#0000ff') }}
	public function shades($colour,$amount=5,$strength=50,$direction=null)	{
		$colour = craft()->colours->validate_hex($colour);
		$shades = [];
		// Force shading direction if the colour is absolute black or white
		if ($colour == '000000' && is_null($direction)) {
			// force lighten
			$strength = 20;
			$direction = 100;
		} else if ($colour === 'ffffff' && is_null($direction)) {
			// force darken
			$strength = 50;
			$direction = -100;
		}

		$amounts = array();

		if($direction > 0) {
			$dec = $direction / 100;
			$v1 = 1;
			$v2 = 0;
		}	else {
			$dec = $direction == 0 || is_null($direction) ? 0.5 : (abs($direction) / 100);
			$v1 = 0;
			$v2 = 1;
		}

		$ratio = ceil($amount * $dec);
		$amounts = array_fill(0, $ratio, $v1);

		if(count($amounts) < $amount) {
			$amounts = array_merge($amounts, array_fill(0, $amount - count($amounts), $v2));
		}

		foreach($amounts as $direction) {
			$randomStrength = floor(rand(1, 100)/100 * $strength);

			if ($direction) {
				// light
				$newColour = $this->lighten($colour, $randomStrength);
				// echo '<span style="display:inline-block; margin-right:5px; width:20px; height:20px; border:1px solid black; position:relative; background-color:'.$newColour.'"></span>'.$newColour.' New colour is about '.$randomStrength.'% <strong>lighter</strong></br>';
			} else {
				// dark
				$newColour = $this->darken($colour, $randomStrength);
				// echo '<span style="display:inline-block; margin-right:5px; width:20px; height:20px; border:1px solid black; position:relative; background-color:'.$newColour.'"></span>'.$newColour.' New colour is about '.$randomStrength.'% <strong>darker</strong></br>';
			}
			array_push($shades,$newColour);

		}

		shuffle($shades);

		return $shades;
	}

  // Return an array of colours that are ligther than the one being queried
	public function lighter_shades($colour, $amount=5, $strange=50) {
		return $this->shades($colour, $amount, $strange, 100);
	}

  // Return an array of colours that are darker than the one being queried
	public function darker_shades($colour, $amount=5, $strange=50) {
		return $this->shades($colour, $amount, $strange, -100);
	}

}
