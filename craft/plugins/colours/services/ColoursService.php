<?php
namespace Craft;

class ColoursService extends BaseApplicationComponent {

  private function _getThemeFields($handle = null) {

    if (!is_null($handle)) {
      // Get all fields with a given handle, so long as it's a themes fieldtype
      $field = craft()->fields->getFieldByHandle($handle);

      if ($field->type == "Colours_Theme") {
        return $field;
      }
    } else {
      // If no handle is given, return the first instance of themes fieldtype.
      $fields = craft()->fields->getAllFields();
      foreach ($fields as $field) {
        if ($field->type == "Colours_Theme") {
          return $field;
        }
      }
    }
  }

  // Pass in the handle of the specific Theme fieldtype you want to get a random colour from.
  public function random($handle = null) {

    $colours = $this->_getThemeFields($handle);

    $settings = explode(',', $colours->settings['colours']);

    shuffle($settings);

    return $settings[0];
  }

  // Pass in the handle of the specific Theme fieldtype you want to get all colours from.
  public function all($handle = null) {

    $colours = $this->_getThemeFields($handle);

    $settings = explode(',', $colours->settings['colours']);

    return $settings;
  }

  // HSL to RGB
  public function hsl2rgb($hsl) {
  	// Fill variables $h, $s, $l by array given.
  	list($h, $s, $l) = $hsl;

  	// If saturation is 0, the given color is grey and only
  	// lightness is relevant.
  	if ($s == 0 ) {
  		$rgb = array($l, $l, $l);
  	}

  	// Else calculate r, g, b according to hue.
  	// Check http://en.wikipedia.org/wiki/HSL_and_HSV#From_HSL for details
  	else
  	{
  		$chroma = (1 - abs(2*$l - 1)) * $s;
  		$h_ 	= $h * 6;
  		$x 		= $chroma * (1 - abs((fmod($h_,2)) - 1)); // Note: fmod because % (modulo) returns int value!!
  		$m = $l - round($chroma/2, 10); // Bugfix for strange float behaviour (e.g. $l=0.17 and $s=1)

  		     if($h_ >= 0 && $h_ < 1) $rgb = array(($chroma + $m), ($x + $m), $m);
  		else if($h_ >= 1 && $h_ < 2) $rgb = array(($x + $m), ($chroma + $m), $m);
  		else if($h_ >= 2 && $h_ < 3) $rgb = array($m, ($chroma + $m), ($x + $m));
  		else if($h_ >= 3 && $h_ < 4) $rgb = array($m, ($x + $m), ($chroma + $m));
  		else if($h_ >= 4 && $h_ < 5) $rgb = array(($x + $m), $m, ($chroma + $m));
  		else if($h_ >= 5 && $h_ < 6) $rgb = array(($chroma + $m), $m, ($x + $m));
  	}

  	return $rgb;
  }

  // Hex to HSL
  function hex2hsl($hex) {
  	//Validate Hex Input
  	$hex = validate_hex($hex);

  	// Split input by color
  	$hex = str_split($hex, 2);

  	// Convert color values to value between 0 and 1
  	$r = (hexdec($hex[0])) / 255;
  	$g = (hexdec($hex[1])) / 255;
  	$b = (hexdec($hex[2])) / 255;

  	return rgb2hsl(array($r,$g,$b));
  }

  // RGB to Hex
  function rgb2hex($rgb) {
  	list($r,$g,$b) = $rgb;
  	$r = round(255 * $r);
  	$g = round(255 * $g);
  	$b = round(255 * $b);
  	return "#".sprintf("%02X",$r).sprintf("%02X",$g).sprintf("%02X",$b);
  }

  // HSL to Hex
  function hsl2hex($hsl) {
  	$rgb = hsl2rgb($hsl);
  	return rgb2hex($rgb);
  }

  // Validate Hex
  function validate_hex($hex) {
  	// Complete patterns like #ffffff or #fff
  	if(preg_match("/^#([0-9a-fA-F]{6})$/", $hex) || preg_match("/^#([0-9a-fA-F]{3})$/", $hex)) {
  		// Remove #
  		$hex = substr($hex, 1);
  	}

  	// Complete patterns without # like ffffff or 000000
  	if(preg_match("/^([0-9a-fA-F]{6})$/", $hex)) {
  		return $hex;
  	}

  	// Short patterns without # like fff or 000
  	if(preg_match("/^([0-9a-f]{3})$/", $hex)) {
  		// Spread to 6 digits
  		return substr($hex, 0, 1) . substr($hex, 0, 1) . substr($hex, 1, 1) . substr($hex, 1, 1) . substr($hex, 2, 1) . substr($hex, 2, 1);
  	}

  	// If input value is invalid return black
  	return "000000";
  }
}
