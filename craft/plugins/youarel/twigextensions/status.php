<?php
namespace Craft;

class status extends \Twig_Extension {

  public function getName() {
    return Craft::t('HTTP Status');
  }

  public function getFunctions() {
    return array(
      'httpStatus' => new \Twig_Function_Method($this, 'getHttpStatus')
    );
  }

  function getHttpStatus() {
    return http_response_code();
  }
}
