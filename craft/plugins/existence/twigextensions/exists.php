<?php
namespace Craft;

use Twig_Extension;
use Twig_Filter_Method;

class exists extends \Twig_Extension {

  public function getName() {
    return Craft::t('Exists');
  }

  public function getFilters() {
    return array(
      'exists' => new Twig_Filter_Method( $this, 'exists')
    );
  }

  // When check for a files existance, just use this file. If it doesn't exist, false is return. Otherwise return the url
  // {{ (images ~ '/logo.jpg')|exists }}
  public function exists($file) {

    if (gettype($file) == 'string') {

      $filePath = '/'.rtrim(ltrim(parse_url($file)["path"], '/'), '/');

      $fileWithDocRoot = $_SERVER['DOCUMENT_ROOT'].$filePath;
      $fileWithGetCwd = getcwd();
      // echo $fileWithGetCwd;

      return is_dir($fileWithDocRoot)      ? $fileWithDocRoot :
             is_dir($fileWithGetCwd)       ? $fileWithGetCwd  :
             file_exists($fileWithDocRoot) ? $fileWithDocRoot :
             file_exists($fileWithGetCwd)  ? $fileWithGetCwd  :
             false;

    }
  }


}
