<?php

return array(
  'test' => strpos(CRAFT_ENVIRONMENT, '.loc') !== false ? '_layouts/test' : 'index',
  'couture' => 'work/index',
  'work/couture' => 'work/index',
);
