<?php

return array(

  // Global
  '*' => array(
    'phpMaxMemoryLimit'                => '2048M',
    'postCpLoginRedirect'              => "entries",
    'addTrailingSlashesToUrls'         => true,
    'generateTransformsBeforePageLoad' => true,
    'autoLoginAfterAccountActivation'  => true,
    'errorTemplatePrefix'              => "_errors/",
    'defaultTemplateExtensions'        => array('twig', 'html'),
    'devMode'                          => true,
    'cooldownDuration'                 => 0,
    'useCompressedJs'                  => true,
    'siteUrl' => array(
      'en' => '/',
      'fi' => '/fi/',
    ),
    'environmentVariables'             => array(
      'systemPath'                     => getcwd(),
      'uploads'                        => '/assets/uploads',
      'images'                         => '/assets/images',
      'sprites'                        => '/assets/images/sprites',
      'css'                            => '/assets/css',
      'js'                             => '/assets/js',
      'videos'                         => '/assets/videos',
    )
  ),

  // Local
  '.loc' => array(
    'environment'                      => 'development',
    'omitScriptNameInUrls'             => true,
    'enableTemplateCaching'            => false,
    'isSystemOn'                       => true,
    'devMode'                          => true,
    'useCompressedJs'                  => false,
    'testToEmailAddress'               => 'mark@marknotton.uk',
  ),

  // Live
  '.com' => array(
    'backupDbOnUpdate'		             => true,
    'environment'                      => 'production',
    'devMode'                          => true,
    'isSystemOn'                       => true,

    // 'siteUrl' => array(
    //   'en' => 'http://www.erikaturunen.com/',
    //   'fn' => 'http://www.erikaturunen.com/fi/',
    // ),
  ),
  // Live
  // '.com/fi' => array(
  //   'backupDbOnUpdate'		             => true,
  //   'environment'                      => 'production',
  //   'devMode'                          => true,
  //   'environmentVariables'             => array(
  //     'systemPath'                     => rtrim(getcwd(),"fi"),
  //     'uploads'                        => '/assets/uploads',
  //     'images'                         => '/assets/images',
  //     'sprites'                        => '/assets/images/sprites',
  //     'css'                            => '/assets/css',
  //     'js'                             => '/assets/js',
  //     'videos'                         => '/assets/videos',
  //   )
  // ),
);
