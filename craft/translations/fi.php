<?php

return array(
  'Work' => 'Tehdä työtä',
  'Couture' => 'Huippumuoti',
  'Recommendations' => 'Suositukset',
  'Instagram' => 'Instagram',
  'Introduction' => 'Esittely',
  'About' => 'Noin',
  'Blog' => 'Blogi',
  'Contact' => 'Ottaa yhteyttä',
  'More' => 'Lisää',
  'page not found' => 'sivua ei löytynyt',
  'All rights reserved' => 'Kaikki oikeudet pidätetään',
  'Developed by' => 'kehittämä',
  'I was visiting your' => 'Olin vierailevat',
  'site, and wanted to get in touch.' => 'sivuston, ja halusi saada yhteyttä.',
  'Previous' => 'Edellinen',
  'More work' => 'Lisää töitä',
  'Next' => 'Seuraava',
  'Show more' => 'Näytä lisää',
  'All blog posts have been loaded' => 'Kaikki blogitekstejä on lastattu'
);
